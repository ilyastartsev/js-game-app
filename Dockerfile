FROM ubuntu:18.04

WORKDIR /code
COPY . /code/

RUN apt-get update

RUN apt-get install nginx -y \ 
  && echo "Configure nginx proxy for ports: 8080->80" \
  && cp conf/default /etc/nginx/sites-available/ \
  && cp conf/nginx.conf /etc/nginx/ 
#  && systemctl restart nginx

RUN echo "Installing node16" \
  && apt-get install curl -y \
  && cd ~ \
  && curl -sL https://deb.nodesource.com/setup_16.x | bash - \
  && apt-get install nodejs -y

RUN echo "App install & build" \
  && cd /code/app \
  && npm install --include=dev \
  && npm run build

RUN echo "Prefer to run app" \
  && cd /code/app \
  && export NODE_ENV=development 
#  && npm start
 
EXPOSE 80

CMD cd app/ ; npm start

