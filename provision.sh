#!/bin/sh

sudo apt-get update

# Installing Nginx
echo "Installing nginx"
apt-get install nginx -y

# Configure nginx proxy for ports: 8080->80
echo "Configure nginx proxy for ports: 8080->80"
cp /vagrant/files/default /etc/nginx/sites-available/default
cp /vagrant/files/nginx.conf /etc/nginx/
systemctl restart nginx

# Installing Git and clone app
echo "Installing Git and clone app"
apt-get install git -y
cd /
git clone https://gitfront.io/r/deusops/JnacRhR4iD8q/2048-game.git

# Installing node16
echo "Installing node16"
cd ~
curl -sL https://deb.nodesource.com/setup_16.x | sudo bash -
apt-get install nodejs

# Aoo install & build
echo "App install & build"
cd /2048-game/
npm
sudo npm install --include=dev
sudo npm run build

# Configure systemd unit
echo "App install & build"
cp /vagrant/files/start.sh /2048-game/
chmod +x /2048-game/start.sh
cp /vagrant/files/app.service /etc/systemd/system

# App-unit enable and start
echo "App install & build"
systemctl enable app.service
systemctl start app.service
