#!/bin/bash

update

# Installing Nginx
echo "Installing nginx"
apt-get install nginx -y

# Configure nginx proxy for ports: 8080->80
echo "Configure nginx proxy for ports: 8080->80"
cp /vagrant/sites-available/default /etc/nginx/sites-available/default
cp /vagrant/nginx.conf /etc/nginx/
systemctl restart nginx

# Installing node16
echo "Installing node16"
-sL https://deb.nodesource.com/setup_16.x | sudo bash -
apt-get install nodejs install

# Installing Git and clone app
echo "Installing Git and clone app"
apt-get install git -y
cd /
git clone https://gitfront.io/r/deusops/JnacRhR4iD8q/2048-game.git

# Aoo install & build
echo "Aoo install & build"
cd /2048-game/
npm install --include=dev
npm run build

# Configure systemd unit
echo "Aoo install & build"
cp /vagrant/start.sh /2048-game/
chmod +x /2048-game/start.sh
cp vagrant/app.service /etc/systemd/system

# App-unit enable and start
echo "Aoo install & build"
systemctl enable app.service
systemctl start app.service
